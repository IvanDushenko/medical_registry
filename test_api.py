"""
Модуль для тестирования АПИ
"""
import requests
from bson.objectid import ObjectId

from data_manager import MongoDB, MongoServer

if __name__ == '__main__':
    with MongoServer():
        response = requests.post('http://127.0.0.1:5000/api/md/', headers={'Content-Type': 'text/plain'},
                                 data=
                                 '''
                                      {
                    "doctors": [
                        {
                            "lpu_code": "820001",
                            "doctor_id": "820001 320117370029",
                            "snils": "071-470-190 42",
                            "lastname": "VLADIMIROVNA",
                            "firstname": "LUBOV",
                            "middlename": "DURNOVA",
                            "birthdate": "1944-10-07",
                            "works": [
                                {
                                    "work_status": "1",
                                    "date_beg": "2000-01-01",
                                    "date_end": "",
                                    "spec_university": "7"
                                }
                            ],
                            "prvs": [
                                {
                                    "spec_code": "76",
                                    "cert_date_issue": "2018-08-31"
                                }
                            ]
                        }
                    ]
                }
                                      '''
                                      )

        # print(response.text)
        assert 'One post: [ObjectId(' in response.text, "requests.post('http://127.0.0.1:5000/api/md/'"
        obj_id = response.text[22:-5]

        response = requests.get('http://127.0.0.1:5000/api/md/')
        # print(response.text)
        assert '"doctor_id": "820001 320117370029"' in response.text, "requests.get('http://127.0.0.1:5000/api/md/')"

        response = requests.get('http://127.0.0.1:5000/api/md/?iddokt=820001 320117370029')
        # print(response.text)
        assert '"doctor_id": "820001 320117370029"' in response.text, "requests.get('http://127.0.0.1:5000/api/md/?iddokt=820001 320117370029')"

        response = requests.get('http://127.0.0.1:5000/api/md/{}'.format(obj_id))
        # print(response.text)
        assert '"doctor_id": "820001 320117370029"' in response.text, "requests.get('http://127.0.0.1:5000/api/md/{}')".format(obj_id)

        MongoDB().db.doctors.find_one_and_delete({'_id': ObjectId(obj_id)})
