"""
АПИ
"""
import json

from bson.objectid import ObjectId
from flask import Flask, request
from flask_restful import Api, Resource

from data_manager import MongoDB

app = Flask(__name__)
api = Api(app)


class MD(Resource):
    """
    Получение регистра всех медработников либо получение конкретного медработника по iddokt
    """

    def get(self):
        if request.args:
            pattern = {'doctor_id': request.args['iddokt']}
            query_result = MongoDB().search_one(pattern)
            return query_result
        else:
            query_result = MongoDB().search_all()
            return {'doctors': list(query_result)}

    def post(self):
        """
        Загрузка реестра медработников
        """
        post_data = [json.loads(request.data.decode())]
        query_result = MongoDB().insert_data(post_data=post_data[0]['doctors'])
        return 'One post: {0}'.format(query_result.inserted_ids)


api.add_resource(MD, '/api/md/')


class GetMDWithID(Resource):
    """
    Получение информации о медработнике по идентификатору
    """
    def get(self, id):
        pattern = {'_id': ObjectId(id)}
        query_result = MongoDB().search_one(pattern)
        return query_result


api.add_resource(GetMDWithID, '/api/md/<string:id>')
