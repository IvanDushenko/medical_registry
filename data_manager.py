"""
Модуль для управления данными
"""
import os
import subprocess
from abc import ABC, abstractmethod
from pathlib import Path

from pymongo import MongoClient

MONGO_PORT = 27017


class Server(ABC):

    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def __enter__(self):
        pass

    @abstractmethod
    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


class MongoServer(Server):

    def __init__(self):
        self.MONGO_PORT = 27017
        self.mongo_server = None

    def __enter__(self):
        try:
            current_path = Path.cwd()
            if 'data' not in os.listdir(current_path):
                Path.mkdir(current_path / 'data')
        except Exception as e:
            raise Exception('Ошибка создания директории для БД:' + str(e))
        try:
            cmd = str(current_path / 'mongodb' / 'bin' / 'mongod.exe') + \
                  ' --dbpath "{}"'.format(current_path / 'data')
            self.mongo_server = subprocess.Popen(cmd, stdout=subprocess.DEVNULL)
        except Exception as e:
            raise Exception('Ошибка запуска сервера БД:' + str(e))
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.mongo_server.kill()


class MongoDB:

    def __init__(self, db_name='md'):
        self.client = MongoClient('127.0.0.1', MONGO_PORT)
        self.db = self.client[db_name]

    def __call__(self):
        return self.db

    def get_db(self):
        return self.db

    def insert_data(self, post_data, collection_name='doctors'):
        result = self.db[collection_name].insert_many(post_data)
        return result

    def search_one(self, pattern=None, filter={"_id": 0}, collection_name='doctors'):
        print(pattern)
        result = self.db[collection_name].find_one(pattern, filter)
        print(result)
        return result

    def search_all(self, pattern=None, filter={"_id": 0}, collection_name='doctors'):
        result = self.db[collection_name].find(pattern, filter)
        return result


if __name__ == '__main__':
    """Для тестирования"""
    with MongoServer():
        MongoDB().db.doctors.drop()
        post_data = [{
            "doctors": [
                {
                    "lpu_code": "820001",
                    "doctor_id": "820001 320117370029",
                    "snils": "071-470-190 42",
                    "lastname": "ВЛАДИМИРОВНА",
                    "firstname": "ЛЮБОВЬ",
                    "middlename": "ДУРНОВА",
                    "birthdate": "1944-10-07",
                    "works": [
                        {
                            "work_status": "1",
                            "date_beg": "2000-01-01",
                            "date_end": "",
                            "spec_university": "7"
                        }
                    ],
                    "prvs": [
                        {
                            "spec_code": "76",
                            "cert_date_issue": "2018-08-31"
                        }
                    ]
                }
            ]
        }]
        result = MongoDB().insert_data(post_data[0]['doctors'])
        print('Inserted: {0}'.format(type(result.inserted_ids[0])))

        print(*MongoDB().db.list_collection_names())
        print(MongoDB().db.md.count_documents({}))

        query = MongoDB().search_all()  # (pattern=pattern, filter=filter)
        for item in query:
            print(item)

        print([i for i in MongoDB()().doctors.find(None, {'_id': 1, 'lpu_code': 1})])
        MongoDB().db.doctors.drop()
