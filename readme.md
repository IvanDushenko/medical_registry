# Сервис регистр медработников
Предназначен для загрузки, хранения, обновления и выгрузки сведений о медработниках
## Используемый стек
Поскольку сервис очень маленький, в качестве фреймворка выбран Flask
Чтобы сервис был устойчив к изменениям структуры данных, в качестве базы данных выбран MongoDB
## Разворачивание
1. Установить (если не установлены) Python 3.9 или старше, venv (командой pip install virtualenv)
2. В консоли перейти в директорию проекта
3. Создать виртуальное окружение командой python -m venv venv
4. Активировать виртуальное окружение (source venv/bin/activate - для linux, venv\Scripts\activate.bat - для Windows)
5. Ввести в консоль команду pip install requirements.txt
6. Скачать архив актуальной версии сервера MongoDB с официального сайта и распаковать его в папку mongodb
7. Запустить файл main.py

