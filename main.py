"""
Менеджер приложения
"""
from api import app
from data_manager import MongoServer

if __name__ == '__main__':
    with MongoServer():
        app.run(debug=True)
